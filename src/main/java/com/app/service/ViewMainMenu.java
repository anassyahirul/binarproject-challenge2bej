package com.app.service;

public class ViewMainMenu implements ViewClass {

    @Override
    public void show() {

        System.out.println("----------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("----------------------------");

        System.out.println("Letakkan file data_sekolah.csv di folder yang sama dengan aplikasi ini,");
        System.out.println("jika sudah, tekan Enter");
        input.nextLine();
        ReadWriteFile readWriteFile = new ReadWriteFile();

        System.out.println("Output akan di generate ke folder \"FileOutput\" yang baru saja dibuat saat menjalankan aplikasi ini");
        System.out.println("Silakan pilih menu");
        System.out.println("1. Generate File untuk menampilkan mean, median, dan modus");
        System.out.println("2. Generate file untuk menampilkan pengelompokkan data");
        System.out.println("3. Generate kedua file");
        System.out.println("0. Exit");

        String fileNameSebaranData = "Result_mean_median_modus.txt";
        String fileNamePengelompokkan = "Result_pengelompokkan.txt";
        ViewDoneMenu done = new ViewDoneMenu();
        switch (promptInput()) {
            case 0 -> {
                System.out.println("Program sedang ditutup");
                System.exit(0);
            }
            case 1 -> {
                readWriteFile.writeMeanMedianMode(fileNameSebaranData);
                done.show();
            }
            case 2 -> {
                System.out.println("Masukkan nilai pembatas untuk pengelompokan data");
                byte divider = promptInput();
                readWriteFile.writeGrouping(fileNamePengelompokkan, divider);
                done.show();
            }
            case 3 -> {
                readWriteFile.writeMeanMedianMode(fileNameSebaranData);
                System.out.println("Masukkan nilai pembatas untuk pengelompokan data");
                byte divider1 = promptInput();
                readWriteFile.writeGrouping(fileNamePengelompokkan, divider1);
                done.show();
            }
            default -> {
                System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                this.show();
            }
        }
    }

}
