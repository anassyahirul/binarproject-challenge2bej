package com.app.service;

import java.util.Scanner;

public interface ViewClass {
    void show();
    Scanner input = new Scanner(System.in);

    default byte promptInput() {
        System.out.print("---->");
        return input.nextByte();
    }
}
