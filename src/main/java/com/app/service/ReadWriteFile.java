package com.app.service;

import com.app.model.Calc;
import com.app.model.Grouping;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ReadWriteFile {
    private final List<Integer> DATA_NILAI = new ArrayList<>();
    private final String RESULT_DIRECTORY = "FileOutput";

    ReadWriteFile () {
        File createDirectory = new File(RESULT_DIRECTORY);
        if (createDirectory.mkdir()) {
            System.out.println("Folder "+ RESULT_DIRECTORY + " berhasil dibuat");
        }

        String fileNameRead = "data_sekolah.csv";
        List<List<String>> data = this.readFile(fileNameRead, ";");
        for (List<String> dataTiapKelas : data) {
            for (int j = 1; j < dataTiapKelas.size(); j++) {
                DATA_NILAI.add(Integer.valueOf(dataTiapKelas.get(j)));
            }
        }
    }

    public List<List<String>> readFile(String fileName, String delimiter) {
        try {
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            List<String> innerList;
            List<List<String>> outerList = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                tempArr = line.split(delimiter);

                innerList = new ArrayList<>(Arrays.asList(tempArr));
                outerList.add(innerList);
            }
            br.close();
            return outerList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void writeMeanMedianMode(String fileName) {
        Calc calc = new Calc(DATA_NILAI);

        try {
            File file = new File(RESULT_DIRECTORY + "/" + fileName);
            if (file.createNewFile()) {
                System.out.println("new File is being created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Pengolahan Nilai:\n\n");
            bwr.write("Berikut hasil sebaran data nilai\n");
            bwr.write("Mean: " + calc.getMean());
            bwr.newLine();
            bwr.write("Median: "+ calc.getMedian());
            bwr.newLine();
            bwr.write("Modus: " + calc.getModus());
            bwr.flush();
            bwr.close();
            System.out.println("Success writing to a file");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }




    public void writeGrouping(String fileName, int divider) {
        Grouping group = new Grouping(DATA_NILAI, divider);
        try {

            File file = new File(RESULT_DIRECTORY + "/" + fileName);
            if (file.createNewFile()) {
                System.out.println("new File is being created");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Pengolahan Nilai:\n\n");
            bwr.write("Pengelompokkan nilai dengan acuan nilai " + divider + " beserta jumlah siswa tiap nilai");
            bwr.newLine();
            bwr.write("-------------------------------------------------------------------");
            bwr.newLine();
            bwr.newLine();

            bwr.write("Daftar Nilai yang kurang dari " + divider);
            bwr.newLine();
            HashMap<Integer, Integer> kurangDari = group.getGroupLessThan();
            if (!kurangDari.isEmpty()){
                for (Integer i : kurangDari.keySet()){
                    bwr.write("Nilai " + i + " berjumlah " + kurangDari.get(i));
                    bwr.newLine();
                }
            } else {
                bwr.write("<Data tidak ditemukan>");
                bwr.newLine();
            }
            bwr.newLine();

            HashMap<Integer, Integer> samaDengan = group.getGroupEqualsTo();
            if (!samaDengan.isEmpty()){
                for (Integer i : samaDengan.keySet()){
                    bwr.write("Terdapat " + samaDengan.get(i) + " siswa yang nilanya " + i);
                    bwr.newLine();
                }
            } else {
                bwr.write("Tidak terdapat siswa yang nilainya " + divider);
                bwr.newLine();
            }
            bwr.newLine();

            bwr.write("Daftar Nilai yang lebih dari " + divider);
            bwr.newLine();
            HashMap <Integer, Integer> lebihDari = group.getGroupMoreThan();
            if (!lebihDari.isEmpty()) {
                for (Integer i : lebihDari.keySet()){
                    bwr.write("Nilai " + i + " berjumlah " + lebihDari.get(i));
                    bwr.newLine();
                }
            } else {
                bwr.write("<Data tidak ditemukan>");
                bwr.newLine();
            }
            bwr.newLine();

            bwr.flush();
            bwr.close();
            System.out.println("Success writing to a file");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


}
