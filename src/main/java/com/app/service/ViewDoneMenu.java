package com.app.service;

public class ViewDoneMenu implements ViewClass{
    @Override
    public void show() {
        ViewMainMenu mainMenu = new ViewMainMenu();
        System.out.println("----------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("----------------------------");

        System.out.println("Pemrosesan telah selesai, silakan pilih menu");
        System.out.println("Output telah di-generate ke folder yang sama");
        System.out.println("Silakan pilih menu");
        System.out.println("1. Kembali ke menu utama");
        System.out.println("0. Exit");

        switch (promptInput()) {
            case 0 -> {
                System.out.println("Program sedang ditutup");
                System.exit(0);
            }
            case 1 -> mainMenu.show();
            default -> {
                System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                this.show();
            }
        }
    }
}
