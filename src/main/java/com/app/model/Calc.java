package com.app.model;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Calc {

    private Integer modus;
    private double mean;
    private double median;

    public Calc(List<Integer> data) {
        this.calcMean(data);
        this.calcModus(data);
        this.calcMedian(data);
    }

    public Integer getModus() {
        return modus;
    }

    public double getMean() {
        return mean;
    }

    public double getMedian() {
        return median;
    }

    public void calcModus(List<Integer> data){
        int maxValue = 0, maxCount = 0;

        for (Integer number : data) {
            int count = 0;
            for (Integer number2 : data) {
                if (Objects.equals(number2, number)){
                    count++;
                }
            }

            if (count > maxCount) {
                maxCount = count;
                maxValue = number;
            }
        }
        this.modus = maxValue;
    }

    public void calcMean(List<Integer> data) {
        int total = 0;
        for (int dataTambah : data)
        {
            total += dataTambah;
        }

        this.mean = total / (double) data.size();
    }

    public void calcMedian(List<Integer> data) {
        double median;
        Collections.sort(data);

        if (data.size() % 2 == 1)
            median = data.get(data.size() / 2);
        else
            median = ((double) (data.get(data.size() / 2) + data.get((data.size() / 2) - 1))) / 2;

        this.median = median;
    }
}
