package com.app.model;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Grouping {

    private final HashMap<Integer, Integer> groupMoreThan = new HashMap<>();
    private final HashMap<Integer, Integer> groupEqualsTo = new HashMap<>();
    private final HashMap<Integer, Integer> groupLessThan = new HashMap<>();

    public Grouping(List<Integer> data, int divider) {
        this.setGroupMoreThan(data, divider);
        this.setGroupEqualsTo(data, divider);
        this.setGroupLessThan(data, divider);
    }

    public HashMap<Integer, Integer> getGroupMoreThan() {
        return groupMoreThan;
    }
    public HashMap<Integer, Integer> getGroupEqualsTo() {
        return groupEqualsTo;
    }
    public HashMap<Integer, Integer> getGroupLessThan() {
        return groupLessThan;
    }

    public void setGroupMoreThan(List<Integer> data, int divider) {

        Integer prevElement = null;
        int count;

        for (Integer number : data) {
            if (!Objects.equals(prevElement, number) && number > divider) {
                count = 0;
                for (Integer number2 : data) {
                    if (Objects.equals(number2, number)) {
                        ++count;
                    }
                }
                this.groupMoreThan.put(number, count);
            }
            prevElement = number;
        }
    }

    public void setGroupEqualsTo(List<Integer> data, int divider) {
        Integer prevElement = null;
        int count;

        for (Integer number : data) {
            if (!Objects.equals(prevElement, number) && number == divider) {
                count = 0;
                for (Integer number2 : data) {
                    if (Objects.equals(number2, number)) {
                        ++count;
                    }
                }
                this.groupEqualsTo.put(number, count);
            }
            prevElement = number;
        }
    }

    public void setGroupLessThan(List<Integer> data, int divider) {

        Integer prevElement = null;
        int count;

        for (Integer number : data) {
            if (!Objects.equals(prevElement, number) && number < divider) {
                count = 0;
                for (Integer number2 : data) {
                    if (Objects.equals(number2, number)) {
                        ++count;
                    }
                }
                this.groupLessThan.put(number, count);
            }
            prevElement = number;
        }
    }
}
